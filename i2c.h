/*
 *
 *  Created on: 25/10/2019
 *  Author: David Ricardo Hern�ndez Pe�aranda
 *
 *  i2c.h
 *
 *  Example usage of SSD1306 library created for MSP-EXP430G2 TI LaunchPad
 *  Complied using TI v18.12.2.LTS
 *
 *  SSD1306 library based on Adafuit's  Arduino library
 *
 */

#ifndef I2C_H_
#define I2C_H_

#include <msp430g2553.h>

unsigned char *PTxData;                                                       // Pointer to TX data
unsigned char TxByteCtr;                                                      // number of bytes to TX

/* ====================================================================
 * I2C Prototype Definitions
 * ==================================================================== */
void i2c_init(void);
void i2c_write(unsigned char, unsigned char *, unsigned char);

#endif /* I2C_H_ */
