/*
 *
 *  Created on: 25/10/2019
 * Author: David Ricardo Hern�ndez Pe�aranda
 *
 *  ssd1306.c
 *
 *  Example usage of SSD1306 library created for MSP-EXP430G2 TI LaunchPad
 *  Complied using TI v18.12.2.LTS
 *
 *  SSD1306 library based on Adafuit's  Arduino library
 *
 */


#include "ssd1306.h"
#include <msp430g2553.h>
#include <stdint.h>
#include "font_5x7.h"
#include "i2c.h"

/* ====================================================================
 * Horizontal Centering Number Array
 * ==================================================================== */
const unsigned char HcenterUL[] = {                                           // Horizontally center number with separators on screen
                               0,                                       // 0 digits, not used but included to size array correctly
                               61,                                      // 1 digit
                               58,                                      // 2 digits
                               55,                                      // 3 digits
                               49,                                      // 4 digits and 1 separator
                               46,                                      // 5 digits and 1 separator
                               43,                                      // 6 digits and 1 separator
                               37,                                      // 7 digits and 2 separators
                               34,                                      // 8 digits and 2 separators
                               31,                                      // 9 digits and 2 separators
                               25                                       // 10 digits and 3 separators
};


/* ====================================================================
 * SSD1306 initialization protocol
 * use: Initialize SSD1306 display, this sends all
 *      the setup commands to configure the display.
 * ==================================================================== */


void ssd1306_init(void) {
    // SSD1306 init sequence
    ssd1306_command(SSD1306_DISPLAYOFF);                                // 0xAE
    ssd1306_command(SSD1306_SETDISPLAYCLOCKDIV);                        // 0xD5
    ssd1306_command(0x80);                                              // the suggested ratio 0x80

    ssd1306_command(SSD1306_SETMULTIPLEX);                              // 0xA8
    ssd1306_command(SSD1306_LCDHEIGHT - 1);

    ssd1306_command(SSD1306_SETDISPLAYOFFSET);                          // 0xD3
    ssd1306_command(0x0);                                               // no offset
    ssd1306_command(SSD1306_SETSTARTLINE | 0x0);                        // line #0
    ssd1306_command(SSD1306_CHARGEPUMP);                                // 0x8D
    ssd1306_command(0x14);                                              // generate high voltage from 3.3v line internally
    ssd1306_command(SSD1306_MEMORYMODE);                                // 0x20
    ssd1306_command(0x00);                                              // 0x0 act like ks0108
    ssd1306_command(SSD1306_SEGREMAP | 0x1);
    ssd1306_command(SSD1306_COMSCANDEC);

    ssd1306_command(SSD1306_SETCOMPINS);                                // 0xDA
    ssd1306_command(0x12);
    ssd1306_command(SSD1306_SETCONTRAST);                               // 0x81
    ssd1306_command(0xCF);

    ssd1306_command(SSD1306_SETPRECHARGE);                              // 0xd9
    ssd1306_command(0xF1);
    ssd1306_command(SSD1306_SETVCOMDETECT);                             // 0xDB
    ssd1306_command(0x40);
    ssd1306_command(SSD1306_DISPLAYALLON_RESUME);                       // 0xA4
    ssd1306_command(SSD1306_NORMALDISPLAY);                             // 0xA6

    ssd1306_command(SSD1306_DEACTIVATE_SCROLL);

    ssd1306_command(SSD1306_DISPLAYON);                                 //--turn on oled panel
}

/* ====================================================================
 * SSD1306 command protocol
 * ==================================================================== */


void ssd1306_command(unsigned char command) {
    buffer[0] = 0x80;
    buffer[1] = command;
    i2c_write(SSD1306_I2C_ADDRESS, buffer, 2);
}

/* ====================================================================
 * SSD1306 clear display function
 * use: Clear Display
 * ==================================================================== */

void ssd1306_clearDisplay(void) {

    ssd1306_setPosition(0, 0);
    uint8_t i;
    uint8_t x;
    for (i = 64; i > 0; i--) {                                          // count down for loops when possible for ULP
        for(x = 16; x > 0; x--) {
            if (x == 1) {
                buffer[x-1] = 0x40;
            } else {
                buffer[x-1] = 0x0;
            }
        }

        i2c_write(SSD1306_I2C_ADDRESS, buffer, 17);
    }
}

/* ====================================================================
 * SSD1306 set position function
 * ==================================================================== */

void ssd1306_setPosition(uint8_t column, uint8_t page) {
    if (column > 128) {
        column = 0;                                                     // constrain column to upper limit
    }

    if (page > 8) {
        page = 0;                                                       // constrain page to upper limit
    }

    ssd1306_command(SSD1306_COLUMNADDR);
    ssd1306_command(column);                                            // Column start address (0 = reset)
    ssd1306_command(SSD1306_LCDWIDTH-1);                                // Column end address (127 = reset)

    ssd1306_command(SSD1306_PAGEADDR);
    ssd1306_command(page);                                              // Page start address (0 = reset)
    ssd1306_command(7);                                                 // Page end address
}

/* ====================================================================
 * SSD1306 print text function
 * use: Print single line of text on row y starting at horizontal
 *      pixel x. There are a total of 7 rows starting at 1. The
 *      horizontal starting position can be from 0 to 127.
 * ==================================================================== */

void ssd1306_printText(uint8_t x, uint8_t y, char *ptString) {
    ssd1306_setPosition(x, y);

    while (*ptString != '\0') {
        buffer[0] = 0x40;

        if ((x + 5) >= 127) {                                           // char will run off screen
            x = 0;                                                      // set column to 0
            y++;                                                        // jump to next page
            ssd1306_setPosition(x, y);                                  // send position change to oled
        }

        uint8_t i;
        for(i = 0; i< 5; i++) {
            buffer[i+1] = font_5x7[*ptString - ' '][i];
        }

        buffer[6] = 0x0;

        i2c_write(SSD1306_I2C_ADDRESS, buffer, 7);
        ptString++;
        x+=6;
    }
}

/* ====================================================================
 * SSD1306 print text block function
 * use: Print a block of text that can span multiple lines,
 *      the code will automagically split up the text on
 *      multiple lines. It will print the text block starting
 *      on row y at horizontal pixel x. There are a total of
 *      7 rows starting at 1. The horizontal starting position
 *      can be from 0 to 127. Store the text block as a char
 *      array. Due to a bug add one extra element to the char
 *      array. For example:
 *      char txtBlock[93] = "This is a long multiline text block.
 *      The code will automatically add extra lines as needed."
 * ==================================================================== */

void ssd1306_printTextBlock(uint8_t x, uint8_t y, char *ptString) {
    char word[12];
    uint8_t i;
    uint8_t endX = x;
    while (*ptString != '\0'){
        i = 0;
        while ((*ptString != ' ') && (*ptString != '\0')) {
            word[i] = *ptString;
            ptString++;
            i++;
            endX += 6;
        }

        word[i++] = '\0';

        if (endX >= 127) {
            x = 0;
            y++;
            ssd1306_printText(x, y, word);
            endX = (i * 6);
            x = endX;
        } else {
            ssd1306_printText(x, y, word);
            endX += 6;
            x = endX;
        }
        ptString++;
    }

}

/* ====================================================================
 * SSD1306 print UI32 function
 * use: Print the 32bit unsigned integer val on row y at horizontal
 *      pixel x. The code automagically adds thousands comma spacing
 *      to enable easy reading of large numbers. Use Hcenter to
 *      horizontally center the number at row y regardless of the value
 *      of x. Hcenter accepts HCENTERUL_ON and HCENTERUL_OFF.
 * ==================================================================== */

void ssd1306_printUI32( uint8_t x, uint8_t y, uint32_t val, uint8_t Hcenter ) {
    char text[14];

    ultoa(val, text);
    if (Hcenter) {
        ssd1306_printText(HcenterUL[digits(val)], y, text);
    } else {
        ssd1306_printText(x, y, text);
    }
}

/* ====================================================================
 *  SSD1306 digicts function
 *  use: converts integers into decimal
 * ==================================================================== */

uint8_t digits(uint32_t n) {
    if (n < 10) {
        return 1;
    } else if (n < 100) {
        return 2;
    } else if (n < 1000) {
        return 3;
    } else if (n < 10000) {
        return 4;
    } else if (n < 100000) {
        return 5;
    } else if (n < 1000000) {
        return 6;
    } else if (n < 10000000) {
        return 7;
    } else if (n < 100000000) {
        return 8;
    } else if (n < 1000000000) {
        return 9;
    } else {
        return 10;
    }
}

/* ====================================================================
 *  SSD1306 ultoa function
 *  use: converts the unsigned long into a character string.
 * ==================================================================== */


void ultoa(uint32_t val, char *string) {
    uint8_t i = 0;
    uint8_t j = 0;
                                                                        // use do loop to convert val to string
    do {
        if (j==3) {                                                     // we have reached a separator position
            string[i++] = ',';                                          // add a separator to the number string
            j=0;                                                        // reset separator indexer thingy
        }
            string[i++] = val%10 + '0';                                 // add the ith digit to the number string
            j++;                                                        // increment counter to keep track of separator placement
    } while ((val/=10) > 0);

    string[i++] = '\0';                                                 // add termination to string
    reverse(string);                                                    // string was built in reverse, fix that
}

/* ====================================================================
 *  SSD1306 reverse function
 *  use: mirror effect.
 * ==================================================================== */

void reverse(char *s)
{
    uint8_t i, j;
    uint8_t c;

    for (i = 0, j = strlen(s)-1; i<j; i++, j--) {
        c = s[i];
        s[i] = s[j];
        s[j] = c;
    }
}
